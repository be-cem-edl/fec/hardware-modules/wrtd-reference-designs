// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mfd/core.h>
#include <linux/fmc.h>

#include "platform_data/fmc-adc-100m14b4cha.h"

enum wrtd_spec150t_adc_dev_offsets {
	FA_SPEC_DBL_ADC_META_START = 0x00000000,
	FA_SPEC_DBL_ADC_META_END   = 0x00000040,
	WRTD_SPEC150T_ADC_FA100_MEM_START = 0x00002000,
	WRTD_SPEC150T_ADC_FA100_MEM_END = 0x00003A00,
	WRTD_SPEC150T_ADC_TRTL_MEM_START = 0x0001E000,
	WRTD_SPEC150T_ADC_TRTL_MEM_END = 0x0003E000,
};

/* MFD devices */
enum spec_fpga_mfd_devs_enum {
	WRTD_SPEC150T_ADC_MFD_TRTL = 0,
	WRTD_SPEC150T_ADC_MFD_FA100,
};

static const struct fmc_adc_platform_data fmc_adc_pdata = {
	.flags = 0,
	.calib_trig_time = 0,
	.calib_trig_threshold = 0,
	.calib_trig_internal = 0,
};

static struct resource wrtd_spec150t_adc_fa100_res[] = {
	{
		.name = "fmc-adc-100m14b4ch-mem",
		.flags = IORESOURCE_MEM,
		.start = WRTD_SPEC150T_ADC_FA100_MEM_START,
		.end = WRTD_SPEC150T_ADC_FA100_MEM_END,
	}, {
		.name = "fmc-adc-100m14b4ch-irq",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 0,
		.end = 0,
	},
	{
		.name = "fmc-adc-dma",
		.flags = IORESOURCE_DMA,
	},
	{
		.name = "fmc-adc-meta",
		.flags = IORESOURCE_MEM,
		.start = FA_SPEC_DBL_ADC_META_START,
		.end = FA_SPEC_DBL_ADC_META_END,
	},
};

static struct resource wrtd_spec150t_adc_trtl_res[] = {
	{
		.name = "mock-turtle-mem",
		.flags = IORESOURCE_MEM,
		.start = WRTD_SPEC150T_ADC_TRTL_MEM_START,
		.end = WRTD_SPEC150T_ADC_TRTL_MEM_END,
	}, {
		.name = "mock-turtle-irq_in",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 1,
	}, {
		.name = "mock-turtle-irq_out",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 2,
	}, {
		.name = "mock-turtle-irq_con",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 3,
	}, {
		.name = "mock-turtle-irq_not",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 4,
	},
};

static const struct mfd_cell wrtd_spec150t_adc_mfd_devs[] = {
	[WRTD_SPEC150T_ADC_MFD_TRTL] = {
		.name = "mock-turtle",
		.platform_data = NULL,
		.pdata_size = 0,
		.num_resources = ARRAY_SIZE(wrtd_spec150t_adc_trtl_res),
		.resources = wrtd_spec150t_adc_trtl_res,
	},
	[WRTD_SPEC150T_ADC_MFD_FA100] = {
		.name = "fmc-adc-100m",
		.platform_data = (void *)&fmc_adc_pdata,
		.pdata_size = sizeof(fmc_adc_pdata),
		.num_resources = ARRAY_SIZE(wrtd_spec150t_adc_fa100_res),
		.resources = wrtd_spec150t_adc_fa100_res,
	},
};


static int wrtd_spec150t_adc_probe(struct platform_device *pdev)
{
	struct resource *rmem, *rdma;
	int irq;
	struct fmc_slot *slot;
	bool present;

	slot = fmc_slot_get(pdev->dev.parent, 1);
	if (IS_ERR(slot)) {
		dev_err(&pdev->dev, "Can't find FMC slot 1 err: %ld\n",
			PTR_ERR(slot));
		return PTR_ERR(slot);
	}
	present = fmc_slot_present(slot);
	fmc_slot_put(slot);
	if (!present) {
		dev_err(&pdev->dev,
			"FMC slot: 1, not present\n");
		return -ENODEV;
	}

	rmem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!rmem) {
		dev_err(&pdev->dev, "Missing memory resource\n");
		return -EINVAL;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "Missing IRQ number\n");
		return -EINVAL;
	}

	rdma = platform_get_resource(pdev, IORESOURCE_DMA, 0);
	if (!rdma) {
		dev_err(&pdev->dev, "Missing DMA engine\n");
		return -EINVAL;
	}
	wrtd_spec150t_adc_fa100_res[2].start = rdma->start;

	/*
	 * We know that this design uses the HTVIC IRQ controller.
	 * This IRQ controller has a linear mapping, so it is enough
	 * to give the first one as input
	 */

	return mfd_add_devices(&pdev->dev, PLATFORM_DEVID_AUTO,
			       wrtd_spec150t_adc_mfd_devs,
			       ARRAY_SIZE(wrtd_spec150t_adc_mfd_devs),
			       rmem, irq, NULL);
}

static int wrtd_spec150t_adc_remove(struct platform_device *pdev)
{
	mfd_remove_devices(&pdev->dev);

	return 0;
}

/**
 * List of supported platform
 */
enum wrtd_spec150t_adc_version {
	WRTD_SPEC150T_ADC_VER = 0,
};

static const struct platform_device_id wrtd_spec150t_adc_id_table[] = {
	{
		.name = "wrtd-spec150t-adc",
		.driver_data = WRTD_SPEC150T_ADC_VER,
	}, {
		.name = "id:000010DC57544E01",
		.driver_data = WRTD_SPEC150T_ADC_VER,
	}, {
		.name = "id:000010dc57544e01",
		.driver_data = WRTD_SPEC150T_ADC_VER,
	},
	{},
};

static struct platform_driver wrtd_spec150t_adc_driver = {
	.driver = {
		.name = "wrtd-spec150t-adc",
		.owner = THIS_MODULE,
	},
	.id_table = wrtd_spec150t_adc_id_table,
	.probe = wrtd_spec150t_adc_probe,
	.remove = wrtd_spec150t_adc_remove,
};
module_platform_driver(wrtd_spec150t_adc_driver);

MODULE_AUTHOR("Federico Vaga <federico.vaga@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
MODULE_DESCRIPTION("Driver for the WRTD SPEC 150T FMC ADC");
MODULE_DEVICE_TABLE(platform, wrtd_spec150t_adc_id_table);

MODULE_SOFTDEP("pre: spec_fmc_carrier mockturtle fmc-adc-100m14b4ch");
