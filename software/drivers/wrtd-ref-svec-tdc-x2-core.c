// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <linux/module.h>
#include<linux/moduleparam.h>
#include <linux/platform_device.h>
#include <linux/mfd/core.h>
#include <linux/mod_devicetable.h>

static char *drivers = "";
module_param(drivers, charp, 0444);
MODULE_PARM_DESC(drivers,
		 "Extra drivers to load: tdc (fmc-tdc)\n");

/* stx2: SVEC TDC x2.  */
enum wrtd_svec_tdc_x2_dev_offsets {
	WRTD_SVEC_TDC_X2_TDC0_MEM_START = 0x0000c000,
	WRTD_SVEC_TDC_X2_TDC0_MEM_END   = 0x0001bFFF,
	WRTD_SVEC_TDC_X2_TDC1_MEM_START = 0x0001c000,
	WRTD_SVEC_TDC_X2_TDC1_MEM_END   = 0x0002bFFF,
	WRTD_SVEC_TDC_X2_TRTL_MEM_START = 0x0003c000,
	WRTD_SVEC_TDC_X2_TRTL_MEM_END   = 0x0005bfff,
};

/* MFD devices */

static struct resource wrtd_svec_tdc_x2_tdc0_res[] = {
	{
		.name  = "fmc-tdc0-mem",
		.flags = IORESOURCE_MEM,
		.start = WRTD_SVEC_TDC_X2_TDC0_MEM_START,
		.end   = WRTD_SVEC_TDC_X2_TDC0_MEM_END,
	}, {
		.name = "fmc-tdc0-irq",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 0,
		.end = 0,
	},
};

static struct resource wrtd_svec_tdc_x2_tdc1_res[] = {
	{
		.name  = "fmc-tdc1-mem",
		.flags = IORESOURCE_MEM,
		.start = WRTD_SVEC_TDC_X2_TDC1_MEM_START,
		.end   = WRTD_SVEC_TDC_X2_TDC1_MEM_END,
	}, {
		.name = "fmc-tdc1-irq",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 1,
		.end = 1,
	},
};

static struct resource wrtd_svec_tdc_x2_trtl_res[] = {
	{
		.name  = "mock-turtle-mem",
		.flags = IORESOURCE_MEM,
		.start = WRTD_SVEC_TDC_X2_TRTL_MEM_START,
		.end   = WRTD_SVEC_TDC_X2_TRTL_MEM_END,
	}, {
		.name = "mock-turtle-irq_in",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 2,
	}, {
		.name = "mock-turtle-irq_out",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 3,
	}, {
		.name = "mock-turtle-irq_con",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 4,
	}, {
		.name = "mock-turtle-irq_not",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 5,
	},
};

#define MFD_CELL_TRTL { \
		.name = "mock-turtle", \
		.platform_data = NULL, \
		.pdata_size = 0, \
		.num_resources = ARRAY_SIZE(wrtd_svec_tdc_x2_trtl_res), \
		.resources = wrtd_svec_tdc_x2_trtl_res, \
	}
#define MFD_CELL_FDT { \
		.name = "fmc-fdelay-tdc", \
		.platform_data = NULL, \
		.pdata_size = 0, \
		.num_resources = ARRAY_SIZE(wrtd_svec_tdc_x2_fdt_res), \
		.resources = wrtd_svec_tdc_x2_fdt_res, \
	}
#define MFD_CELL_TDC0 { \
		.name = "fmc-tdc0", \
		.platform_data = NULL, \
		.pdata_size = 0, \
		.num_resources = ARRAY_SIZE(wrtd_svec_tdc_x2_tdc0_res), \
		.resources = wrtd_svec_tdc_x2_tdc0_res, \
	}
#define MFD_CELL_TDC1 { \
		.name = "fmc-tdc1", \
		.platform_data = NULL, \
		.pdata_size = 0, \
		.num_resources = ARRAY_SIZE(wrtd_svec_tdc_x2_tdc1_res), \
		.resources = wrtd_svec_tdc_x2_tdc1_res, \
	}

static const struct mfd_cell __wrtd_svec_tdc_x2_mfd_devs_base[] = {
	MFD_CELL_TRTL,
};
static const struct mfd_cell __wrtd_svec_tdc_x2_mfd_devs_tdc[] = {
	MFD_CELL_TRTL,
	MFD_CELL_TDC0,
        MFD_CELL_TDC1,
};

static const struct mfd_cell *wrtd_svec_tdc_x2_mfd_cells(const char *extra)
{
	if (strncmp("tdc", extra, 3) == 0)
		return __wrtd_svec_tdc_x2_mfd_devs_tdc;
	else
		return __wrtd_svec_tdc_x2_mfd_devs_base;
}
static unsigned int wrtd_svec_tdc_x2_mfd_count(const char *extra)
{
	if (strncmp("tdc", extra, 3) == 0)
                return 3;
        else
                return 1;
}

static int wrtd_svec_tdc_x2_probe(struct platform_device *pdev)
{
	struct resource *rmem;
	int irq;

	rmem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!rmem) {
		dev_err(&pdev->dev, "Missing memory resource\n");
		return -EINVAL;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "Missing IRQ number\n");
		return -EINVAL;
	}

	/*
	 * We know that this design uses the HTVIC IRQ controller.
	 * This IRQ controller has a linear mapping, so it is enough
	 * to give the first one as input
	 */

	return mfd_add_devices(&pdev->dev, PLATFORM_DEVID_AUTO,
			       wrtd_svec_tdc_x2_mfd_cells(drivers),
			       wrtd_svec_tdc_x2_mfd_count(drivers),
			       rmem, irq, NULL);
}

static int wrtd_svec_tdc_x2_remove(struct platform_device *pdev)
{
	mfd_remove_devices(&pdev->dev);

	return 0;
}

/**
 * List of supported platform
 */
enum wrtd_svec_tdc_x2_version {
	WRTD_SVEC_TDC_X2_VER = 0,
};

static const struct platform_device_id wrtd_svec_tdc_x2_id_table[] = {
	{
		.name = "wrtd-svec-tdc-x2",
		.driver_data = WRTD_SVEC_TDC_X2_VER,
	}, {
		.name = "id:000010DC57544E03",
		.driver_data = WRTD_SVEC_TDC_X2_VER,
	}, {
		.name = "id:000010dc57544e03",
		.driver_data = WRTD_SVEC_TDC_X2_VER,
	},
	{},
};

static struct platform_driver wrtd_svec_tdc_x2_driver = {
	.driver = {
		.name = "wrtd-svec-tdc-x2",
		.owner = THIS_MODULE,
	},
	.id_table = wrtd_svec_tdc_x2_id_table,
	.probe = wrtd_svec_tdc_x2_probe,
	.remove = wrtd_svec_tdc_x2_remove,
};
module_platform_driver(wrtd_svec_tdc_x2_driver);

MODULE_AUTHOR("Federico Vaga <federico.vaga@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
MODULE_DESCRIPTION("Driver for the WRTD SVEC TDC x2");
MODULE_DEVICE_TABLE(platform, wrtd_svec_tdc_x2_id_table);

MODULE_SOFTDEP("pre: svec_fmc_carrier mockturtle");
