// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later
/**
 * @file wrtd-adcin.c
 *
 * ADCIN: trigger coming from the ADC mezzanine.
 *
 */

#include "mockturtle-rt.h"
#include "fmc_adc_aux_trigout.h"

#define ADCIN_NUM_CHANNELS 5

struct wrtd_adcin_dev {
        uint32_t io_addr;
        unsigned char ch;
};

static inline void adcin_writel(const struct wrtd_adcin_dev *dev,
                                uint32_t value, uint32_t reg)
{
        dp_writel(value, dev->io_addr + reg);
}

static inline uint32_t adcin_readl(const struct wrtd_adcin_dev *dev, uint32_t reg)
{
        return dp_readl(dev->io_addr + reg);
}

static inline int adcin_wr_link_up(struct wrtd_adcin_dev *adcin)
{
        return adcin_readl(adcin, AUX_TRIGOUT_STATUS) & AUX_TRIGOUT_WR_LINK;
}

static inline int adcin_wr_time_ready(struct wrtd_adcin_dev *adcin)
{
        return adcin_readl(adcin, AUX_TRIGOUT_STATUS) & AUX_TRIGOUT_WR_VALID;
}

static inline int adcin_wr_sync_timeout(void)
{
        return 0;
}

/**
 * Handles input timestamps from all ADC channels.
 */
static void adcin_input(struct wrtd_adcin_dev *adcin)
{
        uint32_t status = adcin_readl(adcin, AUX_TRIGOUT_STATUS);
        uint32_t mask;
        struct wrtd_event ev;
        int i;

        /* Poll the FIFO and read the timestamp */
        if(!(status & AUX_TRIGOUT_TS_PRESENT))
                return;

        mask = adcin_readl(adcin, AUX_TRIGOUT_TS_MASK_SEC + 0);
        ev.ts.seconds = adcin_readl(adcin, AUX_TRIGOUT_TS_MASK_SEC + 4);
        ev.ts.ns = adcin_readl(adcin, AUX_TRIGOUT_TS_CYCLES) * 8;
        ev.ts.frac = 0;

        for (i = 0; i < ADCIN_NUM_CHANNELS; i++) {
                unsigned ch = adcin->ch + i;

                /* The last channel is the ext trigger with a different mask */
                if ( i == ADCIN_NUM_CHANNELS - 1 ) {
                        if (!(mask & (AUX_TRIGOUT_EXT_MASK >> 32)))
                                continue;
                }
                else if (!(mask & ((AUX_TRIGOUT_CH1_MASK >> 32) << i)))
                        continue;

                zero_id(&ev.id);
                ev.id.c[0] = 'L';
                ev.id.c[1] = 'C';
                ev.id.c[2] = '-';
                ev.id.c[3] = 'I';
                if (ch < 9)
                        ev.id.c[4] = '1' + ch;
                else {
                        ev.id.c[4] = '1';
                        ev.id.c[5] = '0' + ch - 9;
                }

                wrtd_log(WRTD_LOG_MSG_EV_GENERATED,
                         WRTD_LOG_GENERATED_DEVICE + (i * 8),
                         &ev, NULL);

                /* Pass to wrtd.  */
                wrtd_route_in(&ev);
        }
}

static int adcin_init(struct wrtd_adcin_dev *adcin)
{
        pr_debug("adcin initialization complete\n\r");

        return 0;
}
