// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later
/**
 * @file wrtd-rt-adc.c
 *
 */

#include "mockturtle-rt.h"
#include <mockturtle-framework.h>
#include "wrtd-common.h"

#define NBR_CPUS 1
#define CPU_IDX 0
#define NBR_RULES 16
#define NBR_DEVICES 2
#define NBR_ALARMS 1
#define DEVICES_NBR_CHS { 5, 1, 0, 0}
#define DEVICES_CHS_DIR { WRTD_CH_DIR_IN, WRTD_CH_DIR_OUT, 0, 0}
#define APP_ID 0x35B0
#define APP_VER RT_VERSION(WRTD_FW_VER_MAJ, WRTD_FW_VER_MIN)
#define APP_NAME "wrtd-adc"
#define WRTD_NET_TX 1
#define WRTD_NET_RX 1
#define WRTD_LOCAL_TX 1
#define WRTD_LOCAL_RX 1

/* WRPC simulation SW does not support VLANs */
#ifndef SIMULATION
#define WRTD_NET_VLAN
#endif

#include "wrtd-rt-common.h"

#include "wrtd-adcout.c"
#include "wrtd-adcin.c"

static struct wrtd_adcin_dev adcin0 =
{
        .io_addr = 0x1000,
};

static struct wrtd_adcout_dev adcout0 =
{
        .io_addr = 0x0000,
};

static inline int wr_link_up(void)
{
        if (!adcin_wr_link_up(&adcin0))
                return 0;
        return 1;
}

static inline int wr_time_ready(void)
{
        if (!adcin_wr_time_ready(&adcin0))
                return 0;
        return 1;
}

static inline void wr_enable_lock(int enable)
{
        /* No aux clock on ADC boards */
}

static inline int wr_aux_locked(void)
{
        return 1;
}

static inline int wr_sync_timeout(void)
{
        return 0;
}

static int wrtd_local_output(struct wrtd_event *ev, unsigned ch)
{
        return adcout_local_output(&adcout0, ev, ch);
}

static int wrtd_user_init(void)
{
        adcin_init(&adcin0);
        adcout_init(&adcout0);

        wr_enable_lock(0);

        pr_debug("rt-adc firmware initialized.\n\r");

        return 0;
}

static void wrtd_io(void)
{
        adcin_input(&adcin0);
        adcout_output(&adcout0);
}
