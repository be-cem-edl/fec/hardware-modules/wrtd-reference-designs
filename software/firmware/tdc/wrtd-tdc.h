// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef __WRTD_TDC_H
#define __WRTD_TDC_H

#include "wrtd-common.h"

#define BASE_DP_TDC_REGS   0x2000
#define BASE_DP_TDC_CH1    0x5000
#define BASE_DP_TDC_DIRECT 0x8000

#define TDC_NUM_CHANNELS 5

#define NB_ACAM_REGS 11

#define TDC_VCXO_DEFAULT_TUNE 32000

/* Dead time per TDC channel, in 8ns ticks.
   The core adds one more tick to this value */
#define DEFAULT_DEAD_TIME 0xC

struct wrtd_tdc_calibration {
        int32_t offset_tai[TDC_NUM_CHANNELS];
        int32_t offset_coarse[TDC_NUM_CHANNELS];
        int32_t offset_frac[TDC_NUM_CHANNELS];
};

#endif
