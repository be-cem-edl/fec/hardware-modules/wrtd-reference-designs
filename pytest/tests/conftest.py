# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

import pytest

def pytest_addoption(parser):
    parser.addoption("--adc-node", required=True, type=int,
                     help="WRTD node ID for the ADC board")
    parser.addoption("--tdc-node", required=True, type=int,
                     help="WRTD node ID for the TDC board")
    parser.addoption("--fd-node", required=True, type=int,
                     help="WRTD node ID for the FD board")
    parser.addoption("--fmc-adc-id1", required=True, type=int,
                     help="FMC-ADC ID for the ADC in FMC slot 1")
    parser.addoption("--fmc-adc-id2", required=True, type=int,
                     help="FMC-ADC ID for the ADC in FMC slot 2")

def pytest_configure(config):
    pytest.adc_node = config.getoption("--adc-node")
    pytest.tdc_node = config.getoption("--tdc-node")
    pytest.fd_node  = config.getoption("--fd-node")
    pytest.fmc_adc1 = config.getoption("--fmc-adc-id1")
    pytest.fmc_adc2 = config.getoption("--fmc-adc-id2")
