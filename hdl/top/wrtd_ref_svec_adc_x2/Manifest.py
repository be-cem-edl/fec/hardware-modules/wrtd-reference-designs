# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

files = [
    "wrtd_ref_svec_adc_x2.vhd",
    "wrtd_adc_x2_host_map.vhd",
    "wrtd_adc_x2_fmc_map.vhd",
]

fetchto = "../../../dependencies"

modules = {
    "git" : [
        "https://ohwr.org/project/svec.git",
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/vme64x-core.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/mock-turtle.git",
        "https://ohwr.org/project/fmc-adc-100m14b4cha.git",
        "https://ohwr.org/project/ddr3-sp6-core.git",
    ],
}
