-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

-- Do not edit.  Generated on Thu Feb 25 09:17:02 2021 by tgingold
-- With Cheby 1.4.dev0 and these options:
--  --gen-hdl=wrtd_adc_x2_fmc_map.vhd -i wrtd_adc_x2_fmc_map.cheby


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;

entity wrtd_adc_x2_fmc_map is
  port (
    rst_n_i              : in    std_logic;
    clk_i                : in    std_logic;
    wb_i                 : in    t_wishbone_slave_in;
    wb_o                 : out   t_wishbone_slave_out;

    -- FMC ADC0 trigin
    adc0_trigin_i        : in    t_wishbone_master_in;
    adc0_trigin_o        : out   t_wishbone_master_out;

    -- FMC ADC0 trigout
    adc0_trigout_i       : in    t_wishbone_master_in;
    adc0_trigout_o       : out   t_wishbone_master_out;

    -- FMC ADC1 trigin
    adc1_trigin_i        : in    t_wishbone_master_in;
    adc1_trigin_o        : out   t_wishbone_master_out;

    -- FMC ADC1 trigout
    adc1_trigout_i       : in    t_wishbone_master_in;
    adc1_trigout_o       : out   t_wishbone_master_out
  );
end wrtd_adc_x2_fmc_map;

architecture syn of wrtd_adc_x2_fmc_map is
  signal adr_int                        : std_logic_vector(13 downto 2);
  signal rd_req_int                     : std_logic;
  signal wr_req_int                     : std_logic;
  signal rd_ack_int                     : std_logic;
  signal wr_ack_int                     : std_logic;
  signal wb_en                          : std_logic;
  signal ack_int                        : std_logic;
  signal wb_rip                         : std_logic;
  signal wb_wip                         : std_logic;
  signal adc0_trigin_re                 : std_logic;
  signal adc0_trigin_we                 : std_logic;
  signal adc0_trigin_wt                 : std_logic;
  signal adc0_trigin_rt                 : std_logic;
  signal adc0_trigin_tr                 : std_logic;
  signal adc0_trigin_wack               : std_logic;
  signal adc0_trigin_rack               : std_logic;
  signal adc0_trigout_re                : std_logic;
  signal adc0_trigout_we                : std_logic;
  signal adc0_trigout_wt                : std_logic;
  signal adc0_trigout_rt                : std_logic;
  signal adc0_trigout_tr                : std_logic;
  signal adc0_trigout_wack              : std_logic;
  signal adc0_trigout_rack              : std_logic;
  signal adc1_trigin_re                 : std_logic;
  signal adc1_trigin_we                 : std_logic;
  signal adc1_trigin_wt                 : std_logic;
  signal adc1_trigin_rt                 : std_logic;
  signal adc1_trigin_tr                 : std_logic;
  signal adc1_trigin_wack               : std_logic;
  signal adc1_trigin_rack               : std_logic;
  signal adc1_trigout_re                : std_logic;
  signal adc1_trigout_we                : std_logic;
  signal adc1_trigout_wt                : std_logic;
  signal adc1_trigout_rt                : std_logic;
  signal adc1_trigout_tr                : std_logic;
  signal adc1_trigout_wack              : std_logic;
  signal adc1_trigout_rack              : std_logic;
  signal rd_req_d0                      : std_logic;
  signal rd_adr_d0                      : std_logic_vector(13 downto 2);
  signal rd_ack_d0                      : std_logic;
  signal rd_dat_d0                      : std_logic_vector(31 downto 0);
  signal wr_req_d0                      : std_logic;
  signal wr_dat_d0                      : std_logic_vector(31 downto 0);
  signal wr_sel_d0                      : std_logic_vector(3 downto 0);
  signal wr_ack_d0                      : std_logic;
begin

  -- WB decode signals
  adr_int <= wb_i.adr(13 downto 2);
  wb_en <= wb_i.cyc and wb_i.stb;

  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        wb_rip <= '0';
      else
        wb_rip <= (wb_rip or (wb_en and not wb_i.we)) and not rd_ack_int;
      end if;
    end if;
  end process;
  rd_req_int <= (wb_en and not wb_i.we) and not wb_rip;

  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        wb_wip <= '0';
      else
        wb_wip <= (wb_wip or (wb_en and wb_i.we)) and not wr_ack_int;
      end if;
    end if;
  end process;
  wr_req_int <= (wb_en and wb_i.we) and not wb_wip;

  ack_int <= rd_ack_int or wr_ack_int;
  wb_o.ack <= ack_int;
  wb_o.stall <= not ack_int and wb_en;
  wb_o.rty <= '0';
  wb_o.err <= '0';

  -- pipelining for rd-in+rd-out+wr-in+wr-out
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        rd_req_d0 <= '0';
        rd_ack_int <= '0';
        wr_req_d0 <= '0';
        wr_ack_int <= '0';
      else
        rd_req_d0 <= rd_req_int;
        rd_adr_d0 <= adr_int;
        rd_ack_int <= rd_ack_d0;
        wb_o.dat <= rd_dat_d0;
        wr_req_d0 <= wr_req_int;
        wr_dat_d0 <= wb_i.dat;
        wr_sel_d0 <= wb_i.sel;
        wr_ack_int <= wr_ack_d0;
      end if;
    end if;
  end process;

  -- Interface adc0_trigin
  adc0_trigin_tr <= adc0_trigin_wt or adc0_trigin_rt;
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        adc0_trigin_rt <= '0';
        adc0_trigin_wt <= '0';
      else
        adc0_trigin_rt <= (adc0_trigin_rt or adc0_trigin_re) and not adc0_trigin_rack;
        adc0_trigin_wt <= (adc0_trigin_wt or adc0_trigin_we) and not adc0_trigin_wack;
      end if;
    end if;
  end process;
  adc0_trigin_o.cyc <= adc0_trigin_tr;
  adc0_trigin_o.stb <= adc0_trigin_tr;
  adc0_trigin_wack <= adc0_trigin_i.ack and adc0_trigin_wt;
  adc0_trigin_rack <= adc0_trigin_i.ack and adc0_trigin_rt;
  adc0_trigin_o.adr <= ((19 downto 0 => '0') & rd_adr_d0(11 downto 2)) & (1 downto 0 => '0');
  adc0_trigin_o.sel <= wr_sel_d0;
  adc0_trigin_o.we <= adc0_trigin_wt;
  adc0_trigin_o.dat <= wr_dat_d0;

  -- Interface adc0_trigout
  adc0_trigout_tr <= adc0_trigout_wt or adc0_trigout_rt;
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        adc0_trigout_rt <= '0';
        adc0_trigout_wt <= '0';
      else
        adc0_trigout_rt <= (adc0_trigout_rt or adc0_trigout_re) and not adc0_trigout_rack;
        adc0_trigout_wt <= (adc0_trigout_wt or adc0_trigout_we) and not adc0_trigout_wack;
      end if;
    end if;
  end process;
  adc0_trigout_o.cyc <= adc0_trigout_tr;
  adc0_trigout_o.stb <= adc0_trigout_tr;
  adc0_trigout_wack <= adc0_trigout_i.ack and adc0_trigout_wt;
  adc0_trigout_rack <= adc0_trigout_i.ack and adc0_trigout_rt;
  adc0_trigout_o.adr <= ((19 downto 0 => '0') & rd_adr_d0(11 downto 2)) & (1 downto 0 => '0');
  adc0_trigout_o.sel <= wr_sel_d0;
  adc0_trigout_o.we <= adc0_trigout_wt;
  adc0_trigout_o.dat <= wr_dat_d0;

  -- Interface adc1_trigin
  adc1_trigin_tr <= adc1_trigin_wt or adc1_trigin_rt;
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        adc1_trigin_rt <= '0';
        adc1_trigin_wt <= '0';
      else
        adc1_trigin_rt <= (adc1_trigin_rt or adc1_trigin_re) and not adc1_trigin_rack;
        adc1_trigin_wt <= (adc1_trigin_wt or adc1_trigin_we) and not adc1_trigin_wack;
      end if;
    end if;
  end process;
  adc1_trigin_o.cyc <= adc1_trigin_tr;
  adc1_trigin_o.stb <= adc1_trigin_tr;
  adc1_trigin_wack <= adc1_trigin_i.ack and adc1_trigin_wt;
  adc1_trigin_rack <= adc1_trigin_i.ack and adc1_trigin_rt;
  adc1_trigin_o.adr <= ((19 downto 0 => '0') & rd_adr_d0(11 downto 2)) & (1 downto 0 => '0');
  adc1_trigin_o.sel <= wr_sel_d0;
  adc1_trigin_o.we <= adc1_trigin_wt;
  adc1_trigin_o.dat <= wr_dat_d0;

  -- Interface adc1_trigout
  adc1_trigout_tr <= adc1_trigout_wt or adc1_trigout_rt;
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        adc1_trigout_rt <= '0';
        adc1_trigout_wt <= '0';
      else
        adc1_trigout_rt <= (adc1_trigout_rt or adc1_trigout_re) and not adc1_trigout_rack;
        adc1_trigout_wt <= (adc1_trigout_wt or adc1_trigout_we) and not adc1_trigout_wack;
      end if;
    end if;
  end process;
  adc1_trigout_o.cyc <= adc1_trigout_tr;
  adc1_trigout_o.stb <= adc1_trigout_tr;
  adc1_trigout_wack <= adc1_trigout_i.ack and adc1_trigout_wt;
  adc1_trigout_rack <= adc1_trigout_i.ack and adc1_trigout_rt;
  adc1_trigout_o.adr <= ((19 downto 0 => '0') & rd_adr_d0(11 downto 2)) & (1 downto 0 => '0');
  adc1_trigout_o.sel <= wr_sel_d0;
  adc1_trigout_o.we <= adc1_trigout_wt;
  adc1_trigout_o.dat <= wr_dat_d0;

  -- Process for write requests.
  process (rd_adr_d0, wr_req_d0, adc0_trigin_wack, adc0_trigout_wack, adc1_trigin_wack, adc1_trigout_wack) begin
    adc0_trigin_we <= '0';
    adc0_trigout_we <= '0';
    adc1_trigin_we <= '0';
    adc1_trigout_we <= '0';
    case rd_adr_d0(13 downto 12) is
    when "00" =>
      -- Submap adc0_trigin
      adc0_trigin_we <= wr_req_d0;
      wr_ack_d0 <= adc0_trigin_wack;
    when "01" =>
      -- Submap adc0_trigout
      adc0_trigout_we <= wr_req_d0;
      wr_ack_d0 <= adc0_trigout_wack;
    when "10" =>
      -- Submap adc1_trigin
      adc1_trigin_we <= wr_req_d0;
      wr_ack_d0 <= adc1_trigin_wack;
    when "11" =>
      -- Submap adc1_trigout
      adc1_trigout_we <= wr_req_d0;
      wr_ack_d0 <= adc1_trigout_wack;
    when others =>
      wr_ack_d0 <= wr_req_d0;
    end case;
  end process;

  -- Process for read requests.
  process (rd_adr_d0, rd_req_d0, adc0_trigin_i.dat, adc0_trigin_rack, adc0_trigout_i.dat, adc0_trigout_rack, adc1_trigin_i.dat, adc1_trigin_rack, adc1_trigout_i.dat, adc1_trigout_rack) begin
    -- By default ack read requests
    rd_dat_d0 <= (others => 'X');
    adc0_trigin_re <= '0';
    adc0_trigout_re <= '0';
    adc1_trigin_re <= '0';
    adc1_trigout_re <= '0';
    case rd_adr_d0(13 downto 12) is
    when "00" =>
      -- Submap adc0_trigin
      adc0_trigin_re <= rd_req_d0;
      rd_dat_d0 <= adc0_trigin_i.dat;
      rd_ack_d0 <= adc0_trigin_rack;
    when "01" =>
      -- Submap adc0_trigout
      adc0_trigout_re <= rd_req_d0;
      rd_dat_d0 <= adc0_trigout_i.dat;
      rd_ack_d0 <= adc0_trigout_rack;
    when "10" =>
      -- Submap adc1_trigin
      adc1_trigin_re <= rd_req_d0;
      rd_dat_d0 <= adc1_trigin_i.dat;
      rd_ack_d0 <= adc1_trigin_rack;
    when "11" =>
      -- Submap adc1_trigout
      adc1_trigout_re <= rd_req_d0;
      rd_dat_d0 <= adc1_trigout_i.dat;
      rd_ack_d0 <= adc1_trigout_rack;
    when others =>
      rd_ack_d0 <= rd_req_d0;
    end case;
  end process;
end syn;
