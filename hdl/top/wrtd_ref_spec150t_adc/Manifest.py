# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

files = [
    "wrtd_ref_spec150t_adc.vhd",
    "wrtd_adc_host_map.vhd",
    "wrtd_adc_fmc_map.vhd",
]

fetchto = "../../../dependencies"

modules = {
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/mock-turtle.git",
        "https://ohwr.org/project/ddr3-sp6-core.git",
        "https://ohwr.org/project/gn4124-core.git",
        "https://ohwr.org/project/spec.git",
        "https://ohwr.org/project/fmc-adc-100m14b4cha.git",
    ],
}
