# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

files = [
    "wrtd_ref_svec_tdc_x2.vhd",
    "wrtd_tdc_x2_host_map.vhd",
    "wrtd_tdc_x2_fmc_map.vhd",
]

fetchto = "../../../dependencies"

modules = {
    "git" : [
        "https://ohwr.org/project/svec.git",
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/vme64x-core.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/mock-turtle.git",
        "https://ohwr.org/project/fmc-tdc.git",
        "https://ohwr.org/project/fmc-delay-1ns-8cha.git",
        "https://ohwr.org/project/ddr3-sp6-core.git",
    ],
}
