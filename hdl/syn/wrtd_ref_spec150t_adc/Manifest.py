# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

board  = "spec"
target = "xilinx"
action = "synthesis"

syn_device  = "xc6slx150t"
syn_grade   = "-3"
syn_package = "fgg484"
syn_top     = "wrtd_ref_spec150t_adc"
syn_project = "wrtd_ref_spec150t_adc.xise"
syn_tool    = "ise"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../../dependencies"

files = [
    "wrtd_ref_spec150t_adc.ucf",
    "buildinfo_pkg.vhd",
]

modules = {
    "local" : [
        "../common",
        "../../top/wrtd_ref_spec150t_adc",
    ],
}

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
# syn_pre_project_cmd = "make -C ../../../software/firmware/adc"

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass

syn_post_project_cmd = "$(TCL_INTERPRETER) syn_extra_steps.tcl $(PROJECT_FILE)"

spec_base_ucf = ['wr', 'ddr3', 'onewire', 'spi']

ctrls = ["bank3_64b_32b"]
