# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

board  = "svec"
target = "xilinx"
action = "synthesis"

syn_device  = "xc6slx150t"
syn_grade   = "-3"
syn_package = "fgg900"
syn_top     = "wrtd_ref_svec_tdc_fd"
syn_project = "wrtd_ref_svec_tdc_fd.xise"
syn_tool    = "ise"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../../dependencies"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use the
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)

files = [
    "wrtd_ref_svec_tdc_fd.ucf",
    "buildinfo_pkg.vhd",
]

modules = {
    "local" : [
        "../common",
        "../../top/wrtd_ref_svec_tdc_fd",
    ],
}

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
# syn_pre_project_cmd = (
#    "make -C ../../../software/firmware/tdc;"
#    "make -C ../../../software/firmware/fd"
#)

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass

syn_post_project_cmd = "$(TCL_INTERPRETER) syn_extra_steps.tcl $(PROJECT_FILE)"

svec_base_ucf = ['wr', 'led', 'gpio']

ctrls = ["bank4_64b_32b", "bank5_64b_32b"]
