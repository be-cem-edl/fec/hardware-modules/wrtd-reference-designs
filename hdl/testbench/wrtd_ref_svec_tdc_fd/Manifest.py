# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

board      = "svec"
sim_tool   = "modelsim"
sim_top    = "main"
action     = "simulation"
target     = "xilinx"
syn_top    = "wrtd_ref_svec_tdc_fd"
syn_device = "xc6slx150t"
vcom_opt   = "-93 -mixedsvvh"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../../dependencies"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use the
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
#sim_pre_cmd = (
#    "export EXTRA2_CFLAGS='-DSIMULATION';"
#    "make -C ../../../software/firmware/tdc;"
#    "make -C ../../../software/firmware/fd"
#)

include_dirs = [
    fetchto + "/wrtd/hdl/testbench/include",
    fetchto + "/general-cores/sim/",
    fetchto + "/mock-turtle/hdl/testbench/include/",
    fetchto + "/vme64x-core/hdl/sim/vme64x_bfm/",
]

files = [
    "main.sv",
    "dut_env.sv",
    "buildinfo_pkg.vhd",
]

modules = {
    "local" : [
        "../../top/wrtd_ref_svec_tdc_fd",
        "../../syn/common",
    ],
}

ctrls = ["bank4_64b_32b", "bank5_64b_32b"]

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass
