# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

board      = "spec"
sim_tool   = "modelsim"
sim_top    = "main"
action     = "simulation"
target     = "xilinx"
syn_device = "xc6slx150t"
syn_top    = "wrtd_ref_spec150t_adc"
vcom_opt   = "-93 -mixedsvvh"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../../dependencies"

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
#sim_pre_cmd = "EXTRA2_CFLAGS='-DSIMULATION' make -C ../../../software/firmware/adc"

include_dirs = [
    fetchto + "/wrtd/hdl/testbench/include",
    fetchto + "/gn4124-core/hdl/sim/gn4124_bfm",
    fetchto + "/general-cores/sim",
    fetchto + "/mock-turtle/hdl/testbench/include",
    fetchto + "/fmc-adc-100m14b4cha/hdl/testbench/include",
]

files = [
    "main.sv",
    "dut_env.sv",
    "buildinfo_pkg.vhd",
]

modules = {
    "local" : [
        "../../top/wrtd_ref_spec150t_adc",
        "../../syn/common",
    ],
}

ctrls = ["bank3_64b_32b"]

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass
