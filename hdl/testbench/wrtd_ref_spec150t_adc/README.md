.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

Introduction
============

This is a testbench for the SPEC150T-based FMC ADC WRTD reference design.

Dependencies
============

To build this, you will need [hdl-make][1], using commit `968fa87` (or newer), as well as GNU Make.

To run it, you will need Modelsim/Questa. It has been tested with Questa 10.5c on Linux.

Build/Run Instrunctions
=======================

1. If not already done, pull all dependencies using `git submodule update --init` from within the
   WRTD repository.
2. Run `hdlmake` from this directory.
3. Run `make` on the hdlmake-generated Makefile.
4. Run `vsim -c -do run.do`.

[1]: https://www.ohwr.org/projects/hdl-make/wiki
