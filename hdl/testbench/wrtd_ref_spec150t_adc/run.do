# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

vsim -quiet -t 10fs -L unisim work.main -suppress 1270,8617,8683,8684

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

radix -hexadecimal

run -all
