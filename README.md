<!--
SPDX-FileCopyrightText: 2023 CERN

SPDX-License-Identifier: CC-BY-SA-4.0+
-->

# WRTD Reference Designs

This is a collection of WRTD reference designs, produced by and used at CERN. They include gateware,
firmware and top-level Linux kernel drivers. You can use them as is or as a base/reference for your
own WRTD-based designs.

For information on WRTD itself, please visit:
https://ohwr.org/project/wrtd
