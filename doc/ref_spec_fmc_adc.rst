.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _spec150t_ref_adc:

SPEC150T-based FMC-ADC
======================

+---------------------------------------------+------------+
| **Parameter**                               |  **Value** |
+=============================================+============+
|  # of :ref:`Application <wrtd:application>` |      1     |
+---------------------------------------------+------------+
|  Name                                       |  wrtd-adc  |
+---------------------------------------------+------------+
|  Local input Channels                       |      5     |
+---------------------------------------------+------------+
|  Local output Channels                      |      1     |
+---------------------------------------------+------------+
|  Maximum :ref:`Rules <wrtd:rule>`           |     16     |
+---------------------------------------------+------------+
|  Maximum :ref:`Alarms <wrtd:alarm>`         |      1     |
+---------------------------------------------+------------+
|  Average input to Message latency           |    12μs    |
+---------------------------------------------+------------+
|  Average Message to output latency          |    12μs    |
+---------------------------------------------+------------+
|  Can receive Messages over WR               |     YES    |
+---------------------------------------------+------------+
|  Can send Messages over WR                  |     YES    |
+---------------------------------------------+------------+

This is a WRTD :ref:`wrtd:node` based on the `Simple PCIe FMC Carrier (SPEC)
<https://www.ohwr.org/project/spec/wikis/home>`_ and the `FMC ADC 100M 14b 4cha
(FMC-ADC) <https://www.ohwr.org/project/fmc-adc-100m14b4cha/wikis/home>`_.

.. important:: This :ref:`wrtd:node` does not use the standard SPEC, because the
   FPGA (XC6SLX45T) is not large enough for the complete design. Instead it uses
   the pin-compatible XC6SLX150T FPGA (the rest of the board is exactly the
   same, only the FPGA chip is different). This special version is available
   from the manufacturers of the SPEC board upon request.

This :ref:`wrtd:node` provides the possibility to generate WRTD
:ref:`Messages <wrtd:message>` based on trigger events of the FMC-ADC, as well
as to trigger the FMC-ADC from incoming WRTD :ref:`Messages <wrtd:message>`.

.. figure:: graphics/wrtd_ref_design_spec_adc_simple.png
   :name: fig-ref_spec_adc
   :width: 400pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   SPEC150T-based FMC-ADC reference WRTD Node

The architecture of the :ref:`wrtd:node` can be seen in
:numref:`fig-ref_spec_adc`. The user communicates with the :ref:`wrtd:node`
using one of the methods mentioned in :ref:`wrtd:usage`. At the same time,
the user accesses the FMC-ADC core to configure everything not related to WRTD
(acquisition parameters, trigger source selection, data retrieval, etc.)
using the existing
`ADC library <https://www.ohwr.org/project/adc-lib/wikis/home>`_.
Internally, the :ref:`wrtd:node` is running one :ref:`wrtd:application`,
responsible for configuring the WRTD-related aspects of the FMC-ADC.

The five :ref:`Local Input Channels <wrtd:local_channel>` are mapped as follows:

+-------------+---------------------------------+
| **Channel** | **Function**                    |
+=============+=================================+
| ``LC-I1``   | ADC Channel #1 internal trigger |
+-------------+---------------------------------+
| ``LC-I2``   | ADC Channel #2 internal trigger |
+-------------+---------------------------------+
| ``LC-I3``   | ADC Channel #3 internal trigger |
+-------------+---------------------------------+
| ``LC-I4``   | ADC Channel #4 internal trigger |
+-------------+---------------------------------+
| ``LC-I5``   | External trigger input          |
+-------------+---------------------------------+

.. note:: In order for any of the :ref:`Local Input Channels <wrtd:local_channel>`
   to produce an :ref:`wrtd:event`, the user must also properly configure the
   trigger source of the FMC-ADC via the ADC library. This falls outside the
   scope of WRTD library.

The single :ref:`Local Output Channel <wrtd:local_channel>` is mapped as follows:

+-------------+---------------------------------+
| **Channel** | **Function**                    |
+=============+=================================+
| ``LC-O1``   | ADC WRTD trigger output         |
+-------------+---------------------------------+

.. note:: In order for the :ref:`Local Output Channel <wrtd:local_channel>` to
   trigger the FMC-ADC, there is no need to configure anything on the FMC-ADC side,
   the trigger source is always enabled. The user still needs to configure of course
   the necessary :ref:`wrtd:rule` to forward an incoming :ref:`wrtd:event` to this output.

.. hint:: The User Application and the WRTD Application access different and
   separate parts of the FMC-ADC, so there is no danger of accessing the same
   resources simultaneously.
