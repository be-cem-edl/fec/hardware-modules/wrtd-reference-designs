.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _svec_ref_tdc_x2:

SVEC-based TDC x 2
==================

+---------------------------------------------+-------------+
| **Parameter**                               |  **Value**  |
+=============================================+=============+
|  # of :ref:`Application <wrtd:application>` |      1      |
+---------------------------------------------+-------------+
|  Name                                       | wrtd-tdc-x2 |
+---------------------------------------------+-------------+
|  Local input Channels                       |     10      |
+---------------------------------------------+-------------+
|  Local output Channels                      |      0      |
+---------------------------------------------+-------------+
|  Maximum :ref:`Rules <wrtd:rule>`           |     16      |
+---------------------------------------------+-------------+
|  Maximum :ref:`Alarms <wrtd:alarm>`         |      1      |
+---------------------------------------------+-------------+
|  Average input to Message latency           |    20μs     |
+---------------------------------------------+-------------+
|  Average Message to output latency          |    N.A.     |
+---------------------------------------------+-------------+
|  Can receive Messages over WR               |      NO     |
+---------------------------------------------+-------------+
|  Can send Messages over WR                  |     YES     |
+---------------------------------------------+-------------+

This is a WRTD :ref:`wrtd:node` based on the `Simple VME FMC Carrier (SVEC)
<https://www.ohwr.org/project/svec/wikis/home>`_ and the `FMC Time to
Digital Converter (FMC-TDC) <https://www.ohwr.org/project/fmc-tdc/wikis/home>`_.

The basic principle of this :ref:`wrtd:node` is simple: it takes in
external pulses on its FMC-TDC inputs, timestamps them using WR time
and converts them to WRTD :ref:`Messages <wrtd:message>`, to be sent over
the WR network. As such, it can be seen as a "pulse-to-message" converter
with applications in the fields of pulse distribution, trigger synchronisation,
etc.

.. figure:: graphics/wrtd_ref_design_svec_tdc_simple.png
   :name: fig-ref_svec_tdc
   :width: 400pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   SVEC-based FMC-TDC-x2 reference WRTD Node

The architecture of the :ref:`wrtd:node` can be seen in
:numref:`fig-ref_svec_tdc`. The user communicates with the :ref:`wrtd:node`
via the WRTD library, using one of the methods mentioned in :ref:`wrtd:usage`.
Internally, the :ref:`wrtd:node` is running one :ref:`wrtd:application`,
responsible for the two FMC-TDC peripherals.

The ten :ref:`Local Input Channels <wrtd:local_channel>` are mapped as follows:

+-------------+----------------------------------+
| **Channel** | **Function**                     |
+=============+==================================+
| ``LC-I1``   | TDC1 Channel #1                  |
+-------------+----------------------------------+
| ``LC-I2``   | TDC1 Channel #2                  |
+-------------+----------------------------------+
| ``LC-I3``   | TDC1 Channel #3                  |
+-------------+----------------------------------+
| ``LC-I4``   | TDC1 Channel #4                  |
+-------------+----------------------------------+
| ``LC-I5``   | TDC1 Channel #5                  |
+-------------+----------------------------------+
| ``LC-I6``   | TDC2 Channel #1                  |
+-------------+----------------------------------+
| ``LC-I7``   | TDC2 Channel #2                  |
+-------------+----------------------------------+
| ``LC-I8``   | TDC2 Channel #3                  |
+-------------+----------------------------------+
| ``LC-I9``   | TDC2 Channel #4                  |
+-------------+----------------------------------+
| ``LC-I10``  | TDC2 Channel #5                  |
+-------------+----------------------------------+

.. hint:: It is possible for the User Application in :numref:`fig-ref_svec_tdc`
   to access directly the two FMC-TDC cores by means of the FMC-TDC library
   (shown in :numref:`fig-ref_svec_tdc` with dotted lines), in order
   to fine-tune them and/or change their default behaviour. However this should
   be done with extreme care, as it could lead to a race condition between the
   User Application and the WRTD Application running inside the FPGA.

The front-panel LEMO connectors of the SVEC are used as follows:

+---------------+-----------------------------------+
| **Connector** |        **Function**               |
+===============+===================================+
| L1            | WR 1-PPS output                   |
+---------------+-----------------------------------+
| L2            | Not used                          |
+---------------+-----------------------------------+
| L3            | WR external reference clock input |
+---------------+-----------------------------------+
| L4            | WR external 1-PPS input           |
+---------------+-----------------------------------+

The front-panel LED indicators have the following meanings:

+---------------------------+----+----+----------------------------+
|        **Function**       | **LED** |        **Function**        |
+===========================+====+====+============================+
| WR link activity          | 4  |  8 | WR link up                 |
+---------------------------+----+----+----------------------------+
| FD2 clock locked to WR    | 3  |  7 | FD1 clock locked to WR     |
+---------------------------+----+----+----------------------------+
| WR locked                 | 2  |  6 | Not used                   |
+---------------------------+----+----+----------------------------+
| VME bus access            | 1  |  5 | WR 1-PPS                   |
+---------------------------+----+----+----------------------------+

.. hint:: LED colors follow a certain convention:

   * A green LED means ``OK`` or ``on``.
   * A LED that is turned off means ``off`` or ``not active``.
   * A red LED means ``error``.
   * An orange LED signifies ``activity`` (used by LEDs 1, 4 and 5).
