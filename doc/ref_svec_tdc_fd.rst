.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _svec_ref_tdc_fd:

SVEC-based TDC+FD
=================

+----------------------------------------------+------------+------------+
| **Parameter**                                |        **Value**        |
+==============================================+============+============+
| # of :ref:`Applications <wrtd:application>`  |            2            |
+----------------------------------------------+------------+------------+
|                                              | **App #1** | **App #2** |
+----------------------------------------------+------------+------------+
|  Name                                        |  wrtd-tdc  |   wrtd-fd  |
+----------------------------------------------+------------+------------+
|  Local input Channels                        |      5     |      0     |
+----------------------------------------------+------------+------------+
|  Local output Channels                       |      0     |      4     |
+----------------------------------------------+------------+------------+
|  Maximum :ref:`Rules <rule>`                 |     16     |     16     |
+----------------------------------------------+------------+------------+
|  Maximum :ref:`Alarms <alarm>`               |      0     |      1     |
+----------------------------------------------+------------+------------+
|  Average input to Message latency            |     20μs   |     N.A.   |
+----------------------------------------------+------------+------------+
|  Average Message to output latency           |     N.A.   |     20μs   |
+----------------------------------------------+------------+------------+
|  Can receive Messages over WR                |      NO    |     YES    |
+----------------------------------------------+------------+------------+
|  Can send Messages over WR                   |     YES    |      NO    |
+----------------------------------------------+------------+------------+

This is a WRTD :ref:`wrtd:node` based on the `Simple VME FMC Carrier (SVEC)
<https://www.ohwr.org/project/svec/wikis/home>`_, the `FMC Time to Digital
Converter (FMC-TDC) <https://www.ohwr.org/project/fmc-tdc/wikis/home>`_ and
the `FMC Fine Delay generator (FMC-FD)
<https://www.ohwr.org/project/fmc-delay-1ns-8cha/wikis/home>`_.

.. important:: The FMC-TDC should always be attached to "FMC Slot 1" of the
   SVEC, and the FMC-FD should always be attached to "FMC Slot 2". It is not
   necessary though to have both the FMCs attached, the :ref:`wrtd:Node` will
   work if only one of the two (TDC or FD) is present, as long as it is
   connected to the correct FMC slot.

The basic principle of this :ref:`wrtd:node` is simple: it takes in external
pulses on its FMC-TDC inputs, timestamps them using WR time and converts them
to WRTD :ref:`Messages <wrtd:message>`, to be sent over the WR network.
Conversely, the :ref:`wrtd:node` also receives WRTD
:ref:`Messages <wrtd:message>` which are then used to generate pulses at a
predefined moment on one of the FMC-FD outputs. As such, it can be seen as a
"pulse-to-message" and "message-to-pulse" converter with apllications in the
fields of pulse distribution, trigger synchronisation, etc.

.. figure:: graphics/wrtd_ref_design_svec_list_simple.png
   :name: fig-ref_svec_list
   :width: 400pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   SVEC-based TDC+FD reference WRTD Node

The architecture of the :ref:`wrtd:node` can be seen in
:numref:`fig-ref_svec_list`. The user communicates with the :ref:`wrtd:node`
via the WRTD library, using one of the methods mentioned in :ref:`wrtd:usage`.
Internally, the :ref:`wrtd:node` is running two
:ref:`Applications <wrtd:application>`, each on a dedicated CPU. One is
responsible for the FMC-TDC peripheral, while the other handles the FMC-FD.

The five :ref:`Local Input Channels <wrtd:local_channel>` are mapped to the
five inputs of the FMC-TDC. Thus, :ref:`wrtd:event_id` ``LC-I1`` corresponds
to the first FMC-TDC channel, ``LC-I2`` to the second one, and so on,
up to ``LC-I5``.

The four :ref:`Local Output Channels <wrtd:local_channel>` are mapped to the
four outputs of the FMC-FD. :ref:`wrtd:event_id` ``LC-O1`` corresponds to the
first FMC-FD output, and so on, up to ``LC-O4``.

.. hint:: It is possible for the User Application in :numref:`fig-ref_svec_list`
   to access directly the FMC-TDC and FMC-FD cores by means of their respective
   libraries (shown in :numref:`fig-ref_svec_list` with dotted lines), in order
   to fine-tune them and/or change their default behaviour. However this should
   be done with extreme care, as it could lead to a race condition between the
   User Application and the WRTD Applications running inside the FPGA.

The front-panel LEMO connectors of the SVEC are used as follows:

+---------------+-----------------------------------+
| **Connector** |        **Function**               |
+===============+===================================+
| L1            | WR 1-PPS output                   |
+---------------+-----------------------------------+
| L2            | Not used                          |
+---------------+-----------------------------------+
| L3            | WR external reference clock input |
+---------------+-----------------------------------+
| L4            | WR external 1-PPS input           |
+---------------+-----------------------------------+

The front-panel LED indicators have the following meanings:

+---------------------------+----+----+----------------------------+
|        **Function**       | **LED** |        **Function**        |
+===========================+====+====+============================+
| WR link activity          | 4  |  8 | WR link up                 |
+---------------------------+----+----+----------------------------+
| FMC-FD clock locked to WR | 3  |  7 | FMC-TDC clock locked to WR |
+---------------------------+----+----+----------------------------+
| WR locked                 | 2  |  6 | Not used                   |
+---------------------------+----+----+----------------------------+
| VME bus access            | 1  |  5 | WR 1-PPS                   |
+---------------------------+----+----+----------------------------+

.. hint:: LED colors follow a certain convention:

   * A green LED means ``OK`` or ``on``.
   * A LED that is turned off means ``off`` or ``not active``.
   * A red LED means ``error``.
   * An orange LED signifies ``activity`` (used by LEDs 1, 4 and 5).
