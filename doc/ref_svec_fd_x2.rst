.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _svec_ref_fd_x2:

SVEC-based FD x 2
=================

+---------------------------------------------+-------------+
| **Parameter**                               |  **Value**  |
+=============================================+=============+
|  # of :ref:`Application <wrtd:application>` |      1      |
+---------------------------------------------+-------------+
|  Name                                       | wrtd-fd-x2  |
+---------------------------------------------+-------------+
|  Local input Channels                       |      0      |
+---------------------------------------------+-------------+
|  Local output Channels                      |      8      |
+---------------------------------------------+-------------+
|  Maximum :ref:`Rules <wrtd:rule>`           |     16      |
+---------------------------------------------+-------------+
|  Maximum :ref:`Alarms <wrtd:alarm>`         |      1      |
+---------------------------------------------+-------------+
|  Average input to Message latency           |    N.A.     |
+---------------------------------------------+-------------+
|  Average Message to output latency          |    20μs     |
+---------------------------------------------+-------------+
|  Can receive Messages over WR               |     YES     |
+---------------------------------------------+-------------+
|  Can send Messages over WR                  |      NO     |
+---------------------------------------------+-------------+

This is a WRTD :ref:`wrtd:node` based on the `Simple VME FMC Carrier (SVEC)
<https://www.ohwr.org/project/svec/wikis/home>`_
and the `FMC Fine Delay generator (FMC-FD)
<https://www.ohwr.org/project/fmc-delay-1ns-8cha/wikis/home>`_.

The basic principle of this :ref:`wrtd:node` is simple: the :ref:`wrtd:node`
receives WRTD :ref:`Messages <wrtd:message>` which are then used to
generate pulses at a predefined moment on one of the FMC-FD outputs. As
such, it can be seen as a "message-to-pulse" converter with applications in
the fields of pulse distribution, trigger synchronisation, etc

.. figure:: graphics/wrtd_ref_design_svec_fd_simple.png
   :name: fig-ref_svec_fd
   :width: 400pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   SVEC-based FMC-FD-x2 reference WRTD Node

The architecture of the :ref:`wrtd:node` can be seen in
:numref:`fig-ref_svec_fd`. The user communicates with the :ref:`wrtd:node`
via the WRTD library, using one of the methods mentioned in :ref:`wrtd:usage`.
Internally, the :ref:`wrtd:node` is running one :ref:`wrtd:application`,
responsible for the two FMC-FD peripherals.

The eight :ref:`Local Output Channels <wrtd:local_channel>` are mapped as follows:

+-------------+---------------------------------+
| **Channel** | **Function**                    |
+=============+=================================+
| ``LC-O1``   | FD1 Channel #1                  |
+-------------+---------------------------------+
| ``LC-O2``   | FD1 Channel #2                  |
+-------------+---------------------------------+
| ``LC-O3``   | FD1 Channel #3                  |
+-------------+---------------------------------+
| ``LC-O4``   | FD1 Channel #4                  |
+-------------+---------------------------------+
| ``LC-O5``   | FD2 Channel #1                  |
+-------------+---------------------------------+
| ``LC-O6``   | FD2 Channel #2                  |
+-------------+---------------------------------+
| ``LC-O7``   | FD2 Channel #3                  |
+-------------+---------------------------------+
| ``LC-O8``   | FD2 Channel #4                  |
+-------------+---------------------------------+

.. hint:: It is possible for the User Application in :numref:`fig-ref_svec_fd`
   to access directly the two FMC-FD cores by means of the FMC-FD library
   (shown in :numref:`fig-ref_svec_fd` with dotted lines), in order
   to fine-tune them and/or change their default behaviour. However this should
   be done with extreme care, as it could lead to a race condition between the
   User Application and the WRTD Application running inside the FPGA.

The front-panel LEMO connectors of the SVEC are used as follows:

+---------------+-----------------------------------+
| **Connector** |        **Function**               |
+===============+===================================+
| L1            | WR 1-PPS output                   |
+---------------+-----------------------------------+
| L2            | Not used                          |
+---------------+-----------------------------------+
| L3            | WR external reference clock input |
+---------------+-----------------------------------+
| L4            | WR external 1-PPS input           |
+---------------+-----------------------------------+

The front-panel LED indicators have the following meanings:

+---------------------------+----+----+----------------------------+
|        **Function**       | **LED** |        **Function**        |
+===========================+====+====+============================+
| WR link activity          | 4  |  8 | WR link up                 |
+---------------------------+----+----+----------------------------+
| TDC2 clock locked to WR   | 3  |  7 | TDC1 clock locked to WR    |
+---------------------------+----+----+----------------------------+
| WR locked                 | 2  |  6 | Not used                   |
+---------------------------+----+----+----------------------------+
| VME bus access            | 1  |  5 | WR 1-PPS                   |
+---------------------------+----+----+----------------------------+

.. hint:: LED colors follow a certain convention:

   * A green LED means ``OK`` or ``on``.
   * A LED that is turned off means ``off`` or ``not active``.
   * A red LED means ``error``.
   * An orange LED signifies ``activity`` (used by LEDs 1, 4 and 5).

