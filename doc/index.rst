.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _ref_nodes:

====================
WRTD Reference Nodes
====================

This is the documentation for the WRTD reference nodes, provided by (and used at) CERN.

For documentation on WRTD itself, please see the :doc:`WRTD documentation<wrtd:index>`.

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Table of Contents:

   ref_spec_fmc_adc
   ref_svec_tdc_fd
   ref_svec_adc_x2
   ref_svec_fd_x2
   ref_svec_tdc_x2

