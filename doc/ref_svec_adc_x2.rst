.. SPDX-FileCopyrightText: 2022 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _svec_ref_adc_x2:

SVEC-based ADC x 2
==================

+---------------------------------------------+-------------+
| **Parameter**                               |  **Value**  |
+=============================================+=============+
|  # of :ref:`Application <wrtd:application>` |      1      |
+---------------------------------------------+-------------+
|  Name                                       | wrtd-adc-x2 |
+---------------------------------------------+-------------+
|  Local input Channels                       |     10      |
+---------------------------------------------+-------------+
|  Local output Channels                      |      2      |
+---------------------------------------------+-------------+
|  Maximum :ref:`Rules <wrtd:rule>`           |     16      |
+---------------------------------------------+-------------+
|  Maximum :ref:`Alarms <wrtd:alarm>`         |      1      |
+---------------------------------------------+-------------+
|  Average input to Message latency           |    12μs     |
+---------------------------------------------+-------------+
|  Average Message to output latency          |    12μs     |
+---------------------------------------------+-------------+
|  Can receive Messages over WR               |     YES     |
+---------------------------------------------+-------------+
|  Can send Messages over WR                  |     YES     |
+---------------------------------------------+-------------+

This is a WRTD :ref:`node` based on the `Simple VME FMC Carrier (SVEC)
<https://www.ohwr.org/project/svec/wikis/home>`_ and the `FMC ADC 100M 14b 4cha
(FMC-ADC) <https://www.ohwr.org/project/fmc-adc-100m14b4cha/wikis/home>`_.

This :ref:`wrtd:node` provides the possibility to generate WRTD
:ref:`Messages <wrtd:message>` based on trigger events of the FMC-ADC, as well
as to trigger the FMC-ADC from incoming WRTD :ref:`Messages <wrtd:message>`.

.. figure:: graphics/wrtd_ref_design_svec_adc_simple.png
   :name: fig-ref_svec_adc
   :width: 400pt
   :align: center
   :alt: alternate text
   :figclass: align-center

   SVEC-based FMC-ADC-x2 reference WRTD Node

The architecture of the :ref:`wrtd:node` can be seen in
:numref:`fig-ref_svec_adc`. The user communicates with the :ref:`wrtd:node`
using one of the methods mentioned in :ref:`wrtd:usage`. At the same time,
the user accesses the two FMC-ADC cores to configure everything not related to WRTD
(acquisition parameters, trigger source selection, data retrieval, etc.)
using the existing
`ADC library <https://www.ohwr.org/project/adc-lib/wikis/home>`_.
Internally, the :ref:`wrtd:node` is running one :ref:`wrtd:application`,
responsible for configuring the WRTD-related aspects of both FMC-ADC cores.

The ten :ref:`Local Input Channels <wrtd:local_channel>` are mapped as follows:

+-------------+----------------------------------+
| **Channel** | **Function**                     |
+=============+==================================+
| ``LC-I1``   | ADC1 Channel #1 internal trigger |
+-------------+----------------------------------+
| ``LC-I2``   | ADC1 Channel #2 internal trigger |
+-------------+----------------------------------+
| ``LC-I3``   | ADC1 Channel #3 internal trigger |
+-------------+----------------------------------+
| ``LC-I4``   | ADC1 Channel #4 internal trigger |
+-------------+----------------------------------+
| ``LC-I5``   | ADC1 External trigger input      |
+-------------+----------------------------------+
| ``LC-I6``   | ADC2 Channel #1 internal trigger |
+-------------+----------------------------------+
| ``LC-I7``   | ADC2 Channel #2 internal trigger |
+-------------+----------------------------------+
| ``LC-I8``   | ADC2 Channel #3 internal trigger |
+-------------+----------------------------------+
| ``LC-I9``   | ADC2 Channel #4 internal trigger |
+-------------+----------------------------------+
| ``LC-I10``  | ADC2 External trigger input      |
+-------------+----------------------------------+

.. note:: In order for any of the :ref:`Local Input Channels <wrtd:local_channel>`
   to produce an :ref:`wrtd:event`, the user must also properly configure the
   trigger source of the FMC-ADC via the ADC library. This falls outside the
   scope of WRTD library.

The two :ref:`Local Output Channels <wrtd:local_channel>` are mapped as follows:

+-------------+----------------------------------+
| **Channel** | **Function**                     |
+=============+==================================+
| ``LC-O1``   | ADC1 WRTD trigger output         |
+-------------+----------------------------------+
| ``LC-O2``   | ADC2 WRTD trigger output         |
+-------------+----------------------------------+

.. note:: In order for a :ref:`Local Output Channel <wrtd:local_channel>` to
   trigger the FMC-ADC, there is no need to configure anything on the FMC-ADC side,
   the trigger source is always enabled. The user still needs to configure of course
   the necessary :ref:`wrtd:rule` to forward an incoming :ref:`wrtd:event` to this output.

.. hint:: The User Application and the WRTD Application access different and
   separate parts of the FMC-ADC, so there is no danger of accessing the same
   resources simultaneously.

The front-panel LEMO connectors of the SVEC are used as follows:

+---------------+-----------------------------------+
| **Connector** |        **Function**               |
+===============+===================================+
| L1            | WR 1-PPS output                   |
+---------------+-----------------------------------+
| L2            | Not used                          |
+---------------+-----------------------------------+
| L3            | WR external reference clock input |
+---------------+-----------------------------------+
| L4            | WR external 1-PPS input           |
+---------------+-----------------------------------+

The front-panel LED indicators have the following meanings:

+---------------------------+----+----+----------------------------+
|        **Function**       | **LED** |        **Function**        |
+===========================+====+====+============================+
| WR link activity          | 4  |  8 | WR link up                 |
+---------------------------+----+----+----------------------------+
| FMC-ADC2 acquisition      | 3  |  7 | FMC-ADC1 acquisition       |
+---------------------------+----+----+----------------------------+
| WR locked                 | 2  |  6 | Not used                   |
+---------------------------+----+----+----------------------------+
| VME bus access            | 1  |  5 | WR 1-PPS                   |
+---------------------------+----+----+----------------------------+

.. hint:: LED colors follow a certain convention:

   * A green LED means ``OK`` or ``on``.
   * A LED that is turned off means ``off`` or ``not active``.
   * A red LED means ``error``.
   * An orange LED signifies ``activity`` (used by LEDs 1, 4 and 5).
