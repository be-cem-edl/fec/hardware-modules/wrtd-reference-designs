..
   SPDX-FileCopyrightText: 2023 CERN (home.cern)
   SPDX-License-Identifier: CC-BY-SA-4.0+

=========
Changelog
=========

2.1.1 - 2025-02-03
==================

Added
-----
- Update software_build job reference in pytest/.gitlab-ci.yml

2.1.0 - 2023-12-06
==================

Added
-----
- Support for VLAN tags

Fixed
-----
- Endianess for SVEC-ADCx2
- SPEC150T-ADC top-level driver (several issues)
- Onewire signal in slot FMC1 for SVEC-FDx2

Changed
-------
- Improved HDL partitioning and placement
- Use common EDL CI pipelines for building and testing
- Use latest releases for all dependencies

2.0.0 - 2023-01-31
==================

Added
-----
- 3 new SVEC-based designs: dual ADC, dual TDC and dual FDELAY
- CI with pytest-based testing on hardware

Changed
-------
- All reference designs have been moved to this separate repository, and the rest (now called WRTD core)
  has been introduced as a dependency
- Use WRTD core v1.1.0 and also latest releases all other dependencies (except general-cores and wr-cores)
- Versions for gateware, firmware and top-level drivers are now all automatically generated from git tag
- All licences changed to CERN OHLv2 (for gateware), GPLv2 for drivers, LGPLv2.1 for other software and
  CC-BY-SA-4.0 for documentation etc
- Reduced on-chip memory for MockTurtle CPUs according to the needs of each application
- Replaced wishbone crossbars in SPEC150T ADC with auto-generated address decoders from Cheby

1.0.1 - 2021-02-10
==================

Release done before splitting of reference designs from the core repository. For more info, see:
https://ohwr.org/project/wrtd/tags/v1.0.1

1.0.0 - 2019-10-01
==================

Release done before splitting of reference designs from the core repository. For more info, see:
https://ohwr.org/project/wrtd/tags/v1.0.0
